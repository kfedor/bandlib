package bandlib

import (
	"context"
	"fmt"
	"github.com/showwin/speedtest-go/speedtest"
)

func (a *SpeedtestNet) DoTest() (float64, float64, error) {
	fmt.Println("Testing connection speed using Speedtest.net")

	user, err := speedtest.FetchUserInfo()

	if err != nil {
		return 0, 0, err
	}

	serverList, err := speedtest.FetchServerListContext(context.Background(), user)

	if err != nil {
		return 0, 0, err
	}

	urls, err := serverList.FindServer([]int{})

	if err != nil {
		return 0, 0, err
	}

	testsCnt := urls.Len()

	var avgDl float64
	var avgUp float64

	for _, s := range urls {
		fmt.Printf("Testing [%s] %s/%s\n", s.Host, s.Name, s.Country)
		err = s.DownloadTest(false)

		if err != nil {
			return 0, 0, err
		}

		avgDl += s.DLSpeed

		err = s.UploadTest(false)
		if err != nil {
			return 0, 0, err
		}

		avgUp += s.ULSpeed
	}

	return avgDl/float64(testsCnt), avgUp/float64(testsCnt), nil
}
