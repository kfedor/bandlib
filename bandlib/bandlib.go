package bandlib

import (
	"errors"
)

type Bandlib interface {
	DoTest() (float64, float64, error)
}

type Provider int

const (
	Speedtest = 1
	Fast      = 2
)

type FastCom struct{}
type SpeedtestNet struct{}

func GetSpeedProvider(p Provider) (Bandlib, error) {
	var ret Bandlib

	switch p {
	case 1:
		ret = &SpeedtestNet{}
	case 2:
		ret = &FastCom{}
	default:
		return nil, errors.New("unknown speed provider")
	}

	return ret, nil
}
