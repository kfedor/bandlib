package bandlib

import (
	"github.com/gesquive/fast-cli/fast"
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"
	"time"
)

func (a *FastCom) DoTest() (float64, float64, error) {
	fmt.Println("Testing connection speed using Fast.com")
	url := fast.GetDlUrls(3)

	testsCnt := len(url)

	if testsCnt == 0 {
		return 0, 0, errors.New("unable to get download urls")
	}

	var avgDl float64
	var avgUp float64

	for _, u := range url {
		fmt.Printf("Testing [%s]\n", u)

		tDlStart := time.Now()
		dlSize, err := a.download(u)

		if err != nil {
			return 0, 0, err
		}

		tDlEnd := time.Since(tDlStart)

		avgDl += dlSize / tDlEnd.Seconds()

		tUpStart := time.Now()
		upSize := 10000000
		err = a.upload(upSize, u)

		if err != nil {
			return 0, 0, err
		}

		tUpEnd := time.Since(tUpStart)

		avgUp += float64(upSize) / tUpEnd.Seconds()
	}

	return avgDl/float64(testsCnt), avgUp/float64(testsCnt), nil
}

func (a *FastCom) download(url string) (float64, error) {
	response, err := http.Get(url)

	if err != nil {
		return 0, err
	}

	defer response.Body.Close()

	if response.StatusCode != 200 {
		return 0, errors.New("bad http response")
	}

	retData, err := io.ReadAll(response.Body)

	if err != nil {
		return 0, err
	}

	return float64(len(retData)), nil
}

func (a *FastCom) upload(bufSize int, uri string) error {
	var sendBuffer = strings.Repeat("q", bufSize)
	response, err := http.PostForm(uri, url.Values{"key": {sendBuffer}})

	if err != nil {
		return err
	}

	defer response.Body.Close()

	// Read response (response is not required)
	_, err = io.ReadAll(response.Body)

	if err != nil {
		return err
	}

	return nil
}
