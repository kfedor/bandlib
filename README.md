# Bandwidth test lib
URL https://bitbucket.org/kfedor/bandlib

Usage:
```
	# Do import
	import "bitbucket.com/kfedor/bandlib.git/bandlib"

	# Acquire provider 
	provider, err := bandlib.GetSpeedProvider(bandlib.Speedtest)

	# Run bandwidth test 
	dlSpeed, upSpeed, err := provider.DoTest()

	# dlSpeed - download speed in Megabit/sec
	# upSpeed - upload speed in Megabit/sec
	
```

Adding new speed providers:
Implement such interface 

```
	type Bandlib interface {
		DoTest() (float64, float64, error)
	}
```

Testing

```
	cd test
	make
```

or run "go test -v"