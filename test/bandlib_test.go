package bandlib_test

import (
	"bitbucket.com/kfedor/bandlib.git/bandlib"
	"testing"
)

func Test_getprovider(t *testing.T) {
	_, err := bandlib.GetSpeedProvider(bandlib.Speedtest)

	if err != nil {
		t.Fatal(err)
	}

	_, err = bandlib.GetSpeedProvider(bandlib.Fast)

	if err != nil {
		t.Fatal(err)
	}

	_, err = bandlib.GetSpeedProvider(123456)

	if err == nil {
		t.Fatal(err)
	}
}

func Test_fastcom(t *testing.T) {
	p, err := bandlib.GetSpeedProvider(bandlib.Fast)

	if err != nil {
		t.Fatal(err)
	}

	dlSpeed, upSpeed, err := p.DoTest()

	if err != nil {
		t.Fatal(err)
	}

	t.Logf("Download speed: [%.2f] Mbit/sec\n", dlSpeed/1024/1024*8)
	t.Logf("Upload speed: [%.2f] Mbit/sec\n", upSpeed/1024/1024*8)
}

func Test_speedtestnet(t *testing.T) {
	p, err := bandlib.GetSpeedProvider(bandlib.Speedtest)

	if err != nil {
		t.Fatal(err)
	}

	dlSpeed, upSpeed, err := p.DoTest()

	if err != nil {
		t.Fatal(err)
	}

	t.Logf("Download speed: [%.2f] Mbit/sec\n", dlSpeed)
	t.Logf("Upload speed: [%.2f] Mbit/sec\n", upSpeed)
}

func Benchmark_fastcom(b *testing.B) {
	p, err := bandlib.GetSpeedProvider(bandlib.Fast)

	if err != nil {
		b.Fatal(err)
	}

	_, _, err = p.DoTest()

	if err != nil {
		b.Fatal(err)
	}
}

func Benchmark_speedtestnet(b *testing.B) {
	p, err := bandlib.GetSpeedProvider(bandlib.Speedtest)

	if err != nil {
		b.Fatal(err)
	}

	_, _, err = p.DoTest()

	if err != nil {
		b.Fatal(err)
	}
}

