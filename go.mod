module bitbucket.org/kfedor/bandlib

go 1.18

require (
	bitbucket.com/kfedor/bandlib.git v0.0.0-20220318123732-660dbe93c64a
	github.com/gesquive/fast-cli v0.2.10
	github.com/showwin/speedtest-go v1.1.5
)

require (
	github.com/fatih/color v1.9.0 // indirect
	github.com/gesquive/cli v0.2.1 // indirect
	github.com/mattn/go-colorable v0.1.4 // indirect
	github.com/mattn/go-isatty v0.0.11 // indirect
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c // indirect
	golang.org/x/sys v0.0.0-20191026070338-33540a1f6037 // indirect
)
